import torch
import random
import numpy as np
import glob
import matplotlib.pyplot as plt
import cv2 
class CapchaNet(torch.nn.Module):
    def __init__(self):
        super(CapchaNet, self).__init__()
        
        self.conv1 = torch.nn.Conv2d(
            in_channels=1, out_channels=5, kernel_size=10, padding=2)
        self.act1  = torch.nn.Tanh()
        self.pool1 = torch.nn.AvgPool2d(kernel_size=5, stride=2)
        
        self.conv2 = torch.nn.Conv2d(
            in_channels=5, out_channels=25, kernel_size=5, padding=2)
        self.act2  = torch.nn.Tanh()
        self.pool2 = torch.nn.AvgPool2d(kernel_size=2, stride=2)        
        
        self.fc1   = torch.nn.Linear(2520, 1425) #2520 1425
        self.act3  = torch.nn.Tanh()        
        
        self.fc2   = torch.nn.Linear(1425, 120)
        self.act4  = torch.nn.Tanh()   
            
        self.fc3   = torch.nn.Linear(120, 84)
        self.act5  = torch.nn.Tanh()
        
        self.dropout3 = torch.nn.Dropout(p=0.1)
       
        self.fc4   = torch.nn.Linear(84, 22)       
    
    def forward(self, x):        

        x = self.conv1(x)
        x = self.act1(x)
        x = self.pool1(x)        

        x = self.conv2(x)
        x = self.act2(x)
        x = self.pool2(x)
        
        x = x.view(x.size(0), 5, 5*14*36)
        
        x = self.fc1(x)
        x = self.act3(x)

        x = self.fc2(x)
        x = self.act4(x)
        
        x = self.fc3(x)
        x = self.act5(x)
        
        x = self.dropout3(x)
        x = self.fc4(x)  
        
        return x